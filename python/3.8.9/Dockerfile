FROM chriz98/ubuntu:21.04

USER root

# Disable unwanted prompts
ARG DEBIAN_FRONTEND=noninteractive

# Install Python build dependencies and nbconvert dependencies
RUN apt-get update && apt-get -y install --no-install-recommends \
    build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev \
    pandoc texlive-xetex texlive-fonts-recommended

# Update packages and clean caches
RUN apt-get -y dist-upgrade
RUN apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/*

USER user

# Install Python
RUN curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
RUN echo 'export PYENV_ROOT="$HOME/.pyenv"\nexport PATH="$PYENV_ROOT/bin:$PATH"\neval "$(pyenv init --path)"\neval "$(pyenv init -)"\neval "$(pyenv virtualenv-init -)"' >> $HOME/.zshenv

RUN pyenv install 3.8.9
RUN pyenv global 3.8.9

# Install Python packages
RUN pip install -U pip jupyter autopep8 scrapy
